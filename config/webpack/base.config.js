const path = require('path');
const webpack = require('webpack');
const constants = {};
constants.buildFolderRelativeToRoot = 'public/dist';
constants.buildFolder = `../../${constants.buildFolderRelativeToRoot}`;
constants.entryPoint = './src/client/index.js';

const config = {
  entry: {
    main: ['babel-polyfill', constants.entryPoint]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ],
  output: {
    path: path.resolve(__dirname, constants.buildFolder),
    filename: '[name].bundle.js',
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  }
};

module.exports = {
  constants,
  config
};