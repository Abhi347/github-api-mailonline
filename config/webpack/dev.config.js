const merge = require('webpack-merge');
const { config: baseConfig } = require('./base.config.js');

const config = merge(baseConfig, {
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    inline: true,
    contentBase: 'public',
    port: '3001'
  }
});

module.exports = config;
