const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { config: baseConfig, constants } = require('./base.config.js');

const config = merge(baseConfig, {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin([constants.buildFolderRelativeToRoot], {
      root: process.cwd()
    }),
    // Minify JS
    new UglifyJsPlugin({
      sourceMap: false
    }),
    // Minify CSS
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]
});

module.exports = config;
