# Github Test
This app displays various repositories in nodejs organisation and the open issues in each one of them
## Setup
This app requires Node.js (tested on version v8.11.3) to run. Change the working directory to the project and run `npm i` to install the production as well as development dependencies specified in `package.json`.

## Building and Running App
This app uses webpack to build and run the app for both development and production environment. Here's how to build and run for each environment.
### Development Environment
To start the development server run `npm run start-dev`. It'll start the `webpack-dev-server` with hot-reloading enabled.
### Production Environment
For production environment we need to create the bundled js file. For that run `npm run build` and to check with production build, run `npm start`. For testing purpose the production server is assumed to be a local environment. For actual production deployment consider using `forever` package.

## File Structure
### Backend Server
The backend is a simple Express server. It is used for serving the static files from public folder directly. Thus the backend consists of just `app.js` file at the root of the project.
### Frontend Client
The frontend is written in React.JS. The files are available in `./src/client` folder. There're two folders there, each for services and components. Components are React.JS components. A hierarchy is maintained for components as `App->Github->Repo->Issue`. Each component contains the following four things
 - The `index.js` file which contains the actual component class definition.
 - The `index.test.js` file which contains tests written for that particular component.
 - The `__snapshots__` folder which has `jest` snapshot stored for snapshotting matching tests.
 - The child component folder.

### Testing
All the tests are written using `jest` library and can be run using `npm run test`.

### Webpack
The configuration files for webpack are available in `configs/webpack` folder. There're 3 config files. One each for development and production build and a common config which is used in both the environments. Apart from Webpack, various `babel` plugins and polyfills are used to compile the files into a single bundle mostly for supporting older browsers. 