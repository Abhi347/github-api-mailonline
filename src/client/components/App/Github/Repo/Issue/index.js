import React, {
  Component
} from 'react';

class Issue extends Component {
  onClick() {
    window.open(this.props.issue.html_url, '_blank');
  }
  render() {
    return (
      <li onClick={this.onClick.bind(this)}>
        <a className='issue' href={this.props.page || '#'}>{this.props.issue.title}</a>
      </li>
    );
  }
}

export default Issue;