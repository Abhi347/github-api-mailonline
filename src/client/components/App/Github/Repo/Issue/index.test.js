import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Issue from './index';

const issue = {
  html_url: 'https://github.com/nodejs/http-parser/pull/438',
  title: 'test: various small fixes'
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Issue issue={issue} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

test('matches with snapshot', () => {
  const component = renderer.create(
    <Issue issue={issue} />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
