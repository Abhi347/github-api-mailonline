import React, {
  Component
} from 'react';
import Issue from './Issue';
import { getIssues } from '../../../../services/github-service';

class Repo extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      loading: false,
      loaded: false
    };
  }
  onOpenClick() {
    if (this.state.open) {
      this.setState({ open: false });
      return;
    }
    if (!this.state.issues) {
      this.setState({ open: true, loading: true, loaded: false });
      getIssues(this.props.repo)
        .then(issues => {
          this.setState({
            loading: false,
            loaded: true,
            issues
          });
        });
    } else {
      this.setState({
        open: true,
        loading: false,
        loaded: true
      });
    }

  }
  render() {
    if (this.state.open && this.state.loaded) {
      const issueList = this.state.issues.map((issue, index) => {
        return <Issue key={index} issue={issue} />;
      });
      return (
        <li>
          <a className='repo' href={this.props.page || '#'} onClick={this.onOpenClick.bind(this)}>{this.props.repo.name}</a>
          <ul>
            {issueList}
          </ul>
        </li>
      );
    } else if (this.state.open) {
      return (
        <li>
          <a className='repo' href={this.props.page || '#'} onClick={this.onOpenClick.bind(this)}>{this.props.repo.name}</a>
          <ul>
            <li className='issue'>Loading...</li>
          </ul>
        </li>
      );
    } else {
      return (
        <li>
          <a className='repo' href={this.props.page || '#'} onClick={this.onOpenClick.bind(this)}>{this.props.repo.name}</a>
        </li>
      );
    }
  }
}

export default Repo;