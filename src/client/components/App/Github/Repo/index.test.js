import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Repo from './index';

const repo = {
  name: 'http-parser'
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Repo repo={repo} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

test('matches with snapshot', () => {
  const component = renderer.create(
    <Repo repo={repo} />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
