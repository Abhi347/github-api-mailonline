import React, {
  Component
} from 'react';

import Repo from './Repo';
import { getRepos } from '../../../services/github-service';

class Github extends Component {
  constructor() {
    super();
    this.state = {
      loading: true
    };
  }
  componentDidMount() {
    getRepos()
      .then(items => {
        // console.log('items loaded');
        this.setState({
          loading: false,
          items
        });
      });

  }
  render() {
    if (this.state.loading) {
      return (<div >Loading...</div>);
    } else {
      const repoList = this.state.items.map((item, index) => {
        return <Repo key={index} repo={item} />;
      });
      return (<div >
        <ul > {repoList}</ul> </div>
      );
    }
  }
}

export default Github;