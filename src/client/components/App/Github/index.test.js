import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Github from './index';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Github />, div);
  ReactDOM.unmountComponentAtNode(div);
});

test('matches with snapshot', () => {
  const component = renderer.create(
    <Github />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
