import React, { Component } from 'react';
// import { Grid } from '@material-ui/core';
import Github from './Github';

class App extends Component {
  render() {
    return (
      <div>
        <h2>Node.js Github Open issues</h2>
        <Github />
      </div>
    );
  }
}

export default App;
