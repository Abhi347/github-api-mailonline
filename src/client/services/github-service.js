const cache = {
  issues: {}
};

async function getRepos() {
  if(cache.repos) {
    return cache.repos;
  }
  const result = await fetch('https://api.github.com/orgs/nodejs/repos');
  cache.repos = result.json();
  return cache.repos;
}

async function getIssues(repo) {
  if(cache.issues[repo.name]) {
    return cache.issues[repo.name];
  }
  const result = await fetch(`https://api.github.com/repos/nodejs/${repo.name}/issues?state=open`);
  cache.issues[repo.name] = result.json();
  return cache.issues[repo.name];
}

export {
  getIssues,
  getRepos
};